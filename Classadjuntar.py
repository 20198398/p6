class Persona(): 

    def inicializar(self,nom,apel,ced,ed):
        self.nombre=nom
        self.apellido=apel
        self.cedula=int(ced)
        self.edad=int(ed)

    def imprimir(self):
        print("Nombre: ",self.nombre)
        print("Apellido: ",self.apellido)
        print("Cedula: ",self.cedula)
        print("Edad: ",self.edad)

class Profesor(Persona):

    def sueldoprofesor(self,suel):
        self.sueldo=float(suel)

    def impreso(self): 
        print("Sueldo: $", self.sueldo)

print("================================")
personaA=Persona()
personaA.inicializar("Mamerto","Boraz",262728293,34)
Profe=Profesor()
Profe.sueldoprofesor(45000)
personaA.imprimir()
Profe.impreso()
print("================================")

