class A:
    def clasea(self):
        print("Esta es la clase A.")
        print("------------------------------")

class B(A):
    def claseb(self):
        print("Esta es la clase B.")
        print("------------------------------")

class C(B):

    def escribir(self):
        print("Esta es la clase C: ")
        self.write=input("Escribe un dato no numérico: ")

    def imprimir(self):
        print("Dato escrito: ",self.write)


dato=A()
dato.clasea()
dato=B()
dato.claseb()
dato=C()
dato.escribir()
dato.imprimir()

